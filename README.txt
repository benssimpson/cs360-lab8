Ben Simpson
CS 360 
Lab #8

This lab stores comments in and retrieves comments from a Mongo Database.  

Relevant files for the lab are public/index.html, public/javascripts/comments.js, and routes/index.js.

For the creative portion of the lab, I added a color field to each comment.  Each comment will be rendered in the color specified in its color field on the main webpage.


URL of Lab 8 website: http://ec2-54-88-82-132.compute-1.amazonaws.com:3000/

URL of comment rest service (returns a JSON object of all comments in the Mongo Database): http://ec2-54-88-82-132.compute-1.amazonaws.com:3000/comment

URL of Git repository containing project: https://bitbucket.org/benssimpson/cs360-lab8
